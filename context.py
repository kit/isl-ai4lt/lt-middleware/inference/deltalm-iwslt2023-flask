from dataclasses import dataclass


@dataclass
class Manifest:
    pass


@dataclass
class Context:
    manifest: Manifest
    system_properties: dict
