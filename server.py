#!/usr/bin/env python3
from flask import Flask, request
import sys
import sentencepiece as spm
from deltalm_mult_handler import LanguageTranslationHandler
from context import Context
from itertools import islice


def batched(iterable, n):
    "Batch data into tuples of length n. The last batch may be shorter."
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError('n must be at least one')
    it = iter(iterable)
    while batch := tuple(islice(it, n)):
        yield batch


ctx = Context(None, {
    "model_dir": sys.argv[1],
    "gpu_id": 0,
})
translator = LanguageTranslationHandler()
translator.initialize(ctx)


app = Flask(__name__)


#@app.post('/prediction/mt-<lang_pair>')
@app.post('/predictions/mt-<source_lang>,<target_lang>')
def infer(source_lang, target_lang):
    # for sentences in batched(..., 32):
    text = [request.form.get('text')]
    #source_lang, _, target_lang = lang_pair.partition("-")
    if text is None:
        return 400, "Missing input"
    batch = [{'data': s.encode('utf-8')} for s in text]
    x = translator.preprocess(batch)
    y = translator.inference(x, lang_pair=(source_lang, target_lang))
    z = translator.postprocess(y)
    print("\n".join(z))
    return "\n".join(z)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5050)
