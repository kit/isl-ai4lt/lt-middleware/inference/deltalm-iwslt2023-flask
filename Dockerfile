From python:3.9

RUN python3 -m pip install --no-cache-dir fairseq subword-nmt sacremoses sentencepiece Flask

RUN git -C /opt clone https://github.com/microsoft/unilm

WORKDIR /model
RUN curl https://bwsyncandshare.kit.edu/s/7Jb3Zot3mGJLemk/download/deltalm-large.tune.all10.diversifyall.tar.gz \
        | tar -xzf - && mv -i checkpoint_avg_last5.pt model.pt
RUN curl -LO https://bwsyncandshare.kit.edu/s/eDbnDSLBdkMga6D/download/dict_multilingual.txt
RUN curl -LO https://deltalm.blob.core.windows.net/deltalm/spm.model
RUN for lng in en de src tgt; do ln -s dict_multilingual.txt dict.$lng.txt; done
COPY model/truecaser truecaser

# patch bug in fairseq
RUN sed -i '/prefix_allowed_tokens_fn=prefix_allowed_tokens_fn,/d' $(python -c 'import fairseq; print(fairseq.hub_utils.__file__)')

WORKDIR /app
COPY . .

ENV PYTHONPATH="/opt/unilm/deltalm"

CMD ./server.py /model
