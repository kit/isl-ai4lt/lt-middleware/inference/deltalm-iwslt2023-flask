from ts.torch_handler.base_handler import BaseHandler
#from fairseq.models.transformer import TransformerModel
import sentencepiece as spm
from sacremoses import (
    MosesTokenizer,
    MosesTruecaser,
    MosesDetokenizer,
    MosesDetruecaser,
)
import torch
from fairseq.models.transformer import TransformerModel
import logging

logger = logging.getLogger(__name__)

# from fairseq/data/data_utils.py
def collate_tokens(
    values,
    pad_idx,
    eos_idx=None,
    left_pad=False,
    move_eos_to_beginning=False,
    pad_to_length=None,
    pad_to_multiple=1,
    pad_to_bsz=None,
):
    """Convert a list of 1d tensors into a padded 2d tensor."""
    size = max(v.size(0) for v in values)
    size = size if pad_to_length is None else max(size, pad_to_length)
    if pad_to_multiple != 1 and size % pad_to_multiple != 0:
        size = int(((size - 0.1) // pad_to_multiple + 1) * pad_to_multiple)

    batch_size = len(values) if pad_to_bsz is None else max(len(values), pad_to_bsz)
    res = values[0].new(batch_size, size).fill_(pad_idx)

    def copy_tensor(src, dst):
        assert dst.numel() == src.numel()
        if move_eos_to_beginning:
            if eos_idx is None:
                # if no eos_idx is specified, then use the last token in src
                dst[0] = src[-1]
            else:
                dst[0] = eos_idx
            dst[1:] = src[:-1]
        else:
            dst.copy_(src)

    for i, v in enumerate(values):
        copy_tensor(v, res[i][size - len(v) :] if left_pad else res[i][: len(v)])
    return res


class LanguageTranslationHandler(BaseHandler):

    def __init__(self):
        self._context = None
        self.initialized = False
        self.model = None
        self.device = None

    def initialize(self, context):
        self._context = context
        self.initialized = True
        self.manifest = context.manifest

        properties = context.system_properties
        model_dir = properties.get("model_dir")
        source_lang = properties.get('source_lang', 'en')
        target_lang = properties.get('target_lang', 'de')

        self.device = torch.device(
            "cuda:" + str(properties.get("gpu_id"))
            if torch.cuda.is_available() and properties.get("gpu_id") is not None
            else "cpu")

        ##read configs for the model_name, bpe etc. from setup_config.json
        #setup_config_path = os.path.join(model_dir, "setup_config.json")
        #if os.path.isfile(setup_config_path):
        #    with open(setup_config_path) as setup_config_file:
        #        self.setup_config = json.load(setup_config_file)
        #else:
        #    logger.warning('Missing the setup_config.json file.')

        #  load the model
        self.model = TransformerModel.from_pretrained(
            model_dir,
            checkpoint_file='model.pt',
            #checkpoint_file='/project/OML/dliu/iwslt2023/model/mt/deltalm-large.tune.bilingual.de.diversify/checkpoint_best.pt',
            data_name_or_path=model_dir,
            #tokenizer='moses',
            #bpe=self.setup_config["bpe"],
            pretrained_deltalm_checkpoint="",
            #user_dir="/project/iwslt2014c/MT/user/cmullov/projects/ja-deltalm-bcai/src/userdir",
            #user_dir="unilm/deltalm/deltalm/models",
            user_dir="/opt/unilm/deltalm",
            source_lang=source_lang, target_lang=target_lang,
        )
        #model = TransformerModel.from_pretrained(
        #    root_dir,
        #    checkpoint_file=model_filename,
        #    data_name_or_path=data_path,
        #    bpe='sentencepiece',
        #    #user_dir='src/userdir',
        #    user_dir=user_dir,
        #    pretrained_deltalm_checkpoint="",
        #    export=True,
        #    #user_dir='../../ja-deltalm-bcai/src/userdir',
        #    sentencepiece_model=args.spm
        #)
        self.model.to(self.device)
        self.model.eval()

        with open(model_dir + '/dict.src.txt', 'r') as f:
            self.src_dict = {w.strip(): i for i, w in enumerate(f)}
        with open(model_dir + '/dict.tgt.txt', 'r') as f:
            self.tgt_dict = {i: w.strip() for i, w in enumerate(f)}
        self.sp = spm.SentencePieceProcessor()
        self.sp.Load(model_dir + '/spm.model')
        self.truecaser = MosesTruecaser(model_dir + '/truecaser')
        self.tokenizer = MosesTokenizer()
        self.detruecaser = MosesDetruecaser()
        self.detokenizer = MosesDetokenizer()
        self.initialized = True

#    def preprocess(self, data):
#        #textInput = []
#        #for row in data:
#        #    text = row.get("data") or row.get("body")
#        #    decoded_text = text.decode('utf-8')
#        #    textInput.append(decoded_text)
#        #return textInput
#        def _process(sentence):
#            textpp = self.tokenizer.tokenize(sentence, escape=False, return_str=True)
#            textpp = self.truecaser.truecase(textpp, return_str=True)
#            textpp = self.detokenizer.detokenize(textpp.split(" "), unescape=False)
#            tokenized = self.sp.Encode(textpp, out_type=str)
#            vals = [self.src_dict[tok] for tok in tokenized]
#            return torch.LongTensor(vals)
#        values = []
#        for row in data:
#            text = row.get("data") or row.get("body") or row.get("text")
#            decoded_text = text.decode('utf-8')
#            val = _process(decoded_text)
#            values.append(val)
#        tokens = collate_tokens(values,
#                                pad_idx=1,
#                                eos_idx=2,
#                                left_pad=True,
#                                move_eos_to_beginning=False,
#                                pad_to_length=None,
#                                pad_to_multiple=1,
#                                pad_to_bsz=None)
#        tokens = torch.cat([tokens, torch.full((len(tokens), 1), 2)], 1)
#        lengths = (tokens != self.src_dict['<pad>']).sum(1)
#        return {
#            'net_input': {
#                'src_tokens': tokens,
#                'src_lengths': lengths,
#            }
#        }
    def preprocess(self, data):
        def _process(sentence):
            textpp = self.tokenizer.tokenize(sentence, escape=False, return_str=True)
            textpp = self.truecaser.truecase(textpp, return_str=True)
            textpp = self.detokenizer.detokenize(textpp.split(" "), unescape=False)
            tokenized = self.sp.Encode(textpp, out_type=str)
            return " ".join(tokenized)
        values = []
        for row in data:
            text = row.get("data") or row.get("body") or row.get("text")
            decoded_text = text.decode('utf-8')
            val = _process(decoded_text)
            values.append(val)
        return values

    def inference(self, data, *args, **kwargs):
        source_lang, target_lang = kwargs.get('lang_pair', ('en', 'de'))
        self.model.task.dicts[target_lang] = self.model.task.dicts['en']
        self.model.task.args.target_lang = target_lang
        self.model.task.args.source_lang = source_lang
        with torch.no_grad():
            translation = self.model.translate(data)
        logger.info("Model translated: %s", translation)
        return translation

    def postprocess(self, data):
        def _process(sentence):
            pp = sentence.replace(" ", "").replace("\u2581", " ").strip()
            _, _, pp = pp.partition(" ")
            pp = self.detruecaser.detruecase(pp, return_str=True)
            return pp
        return [_process(x) for x in data]
